//
//  FontStyleGuide.h
//  
//
//  Created by Sapana Ranipa on 23/12/15.
//
//

#import <Foundation/Foundation.h>


@interface FontStyleGuide : NSObject

// Color
+(UIColor *) ColorDefault;

// Font
+ (UIFont *)fontRegular;

+ (UIFont *)fontRegularLight;
+ (UIFont *)fontRegular:(CGFloat)size;

+ (UIFont *)fontRegularBold;
+ (UIFont *)fontRegularBold:(CGFloat)size;
@end
