//  ConstantPath.h
//  SDPAClient
//  Created by Sapana Ranipa on 23/12/15.
//  Copyright © 2015 Sapana Ranipa. All rights reserved.

#ifndef ConstantPath_h
#define ConstantPath_h

#ifdef DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#   define DLog(...)
#endif
#define APPDELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
// Googleapis URL
#define Address_URL @"https://maps.googleapis.com/maps/api/geocode/json?"
#define AutoComplete_URL @"https://maps.googleapis.com/maps/api/place/autocomplete/json?"

//Googleapis Keys
//#define Google_Map_Key @"AIzaSyBYPD_UjCam-NR0Ccw3UTMmlNTSblv7Flk"
//#define GOOGLE_KEY @"AIzaSyCNv9UIWWYtY5Xb2xiKMoE28XKeEAXsK-o"


#define Google_Map_Key @"AIzaSyCJ3sM5w60OqM_2C8oUep5dBYHT9Y-IBU4"
#define GOOGLE_KEY @"AIzaSyCJ3sM5w60OqM_2C8oUep5dBYHT9Y-IBU4"

// Base URL

#define API_URL @"http://52.24.145.28/carrier/"
#define SERVICE_URL @"http://52.24.145.28/"
#define PRIVACY_URL @"http://samedaypickupautotransport.com/legal/t-and-c-client/"
#define HELP_URL @"http://samedaypickupautotransport.com/contact/"

// PathName For WebServices

#define P_LOGIN @"login"
#define P_FORGET_PASSWORD @"app/forget_password"
#define P_GET_STATE @"app/get_state"
#define P_REGISTER @"register"
#define P_OTP @"confirm_otp"
#define P_GET_AVAILABLE_BIDS @"get_arround_request"
#define P_BIDDING_ON_REQUEST @"bidding_on_reqeust"
#define P_GET_BIDS @"get_bidding_list"
#define P_GET_REQUEST @"get_request"
#define P_ON_PICKUP_WAY @"pickup_on_way"
#define P_REACHED_ON_PICKUP_LOCATION @"reached_pickup"
#define P_PICKUP_DONE @"pickedup"
#define P_ON_DELIVERY_WAY @"ondelivery_way"
#define P_REACHED_ON_DELIVERY_LOCATION @"reached_delivery"
#define P_DELIVERY_DONE @"delivery_done"
#define P_UPDATE_PROFILE @"update"
#define P_GET_HISTORY @"get_history"
#define P_LOGOUT @"logout"
#define P_GET_HELP @"app/get_help"

// SegueName For Navigation
#define LOGIN_SUCCESS @"segueToLoginSuccess"
#define DIRECT_LOGIN @"SegueToDirectLogin"
#define REGISTER_SUCCESS @"SegueToSuccessRegister"
#define SEGUE_TO_PROFILE @"segueToProfileVC"
#define SEGUE_TO_MYBIDS @"segueToMyBids"
#define SEGUE_TO_HISTORY @"segueToHistoryVC"
#define SEGUE_TO_HELP @"segueToHelpVC"
#define SEGUE_TO_ONGOING_BID @"segueToOnGoingJobs"
#define SEGUE_TO_FEEDBACK @"segueToFeedBack"
#define SEGUE_TO_UNWIND @"SegueToHome"
#define SEGUE_TO_TERMS @"segueToTerms"
#endif
