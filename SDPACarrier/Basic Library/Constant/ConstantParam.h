//
//  ConstantParam.h
//  SDPAClient
//
//  Created by Sapana Ranipa on 23/12/15.
//  Copyright © 2015 Sapana Ranipa. All rights reserved.
//

#ifndef ConstantParam_h
#define ConstantParam_h

// define all require ParamName

#define PARAM_DEVICE_TYPE @"device_type"
#define PARAM_DEVICE_TOKEN @"device_token"
#define PARAM_PICTURE @"picture"
#define PARAM_NAME @"name"
#define PARAM_USER_NAME @"username"
#define PARAM_EMAIL @"email"
#define PARAM_PHONE @"phone"
#define PARAM_PASSWORD @"password"
#define PARAM_LICENCE @"mc_license"
#define PARAM_PLATE @"licence_plate"
#define PARAM_TYPE @"type"
#define PARAM_TOKEN @"token"
#define PARAM_ID @"id"
#define PARAM_OTP @"otp"
#define PARAM_LATITUDE @"latitude"
#define PARAM_LONGITUDE @"longitude"
#define PARAM_DISTANCE @"distance"
#define PARAM_STATE_ID @"state_id"
#define PARAM_REQUEST_ID @"request_id"
#define PARAM_BID_PRICE @"bidding_price"
#define PARAM_BID_TIME @"pickup_time"
#define PARAM_BID_PICKUP_DATE @"pickup_date"
#define PARAM_BID_DELIVERY_DATE @"delivery_time"
#define PARAM_CLIENT_ID @"client_id"
#define PARAM_DESCRIPTION @"description"
#define PREF_REQUEST_ID @"request_id"
#define PARAM_OLD_PASSWORD @"old_password"
#define PARAM_NEW_PASSWORD @"new_password"

// define all require PreferenceName
#define PREF_DEVICE_TOKEN @"device_token"
#define PREF_IS_LOGIN @"is_login"
#define PREF_LOGIN_OBJECT @"login_object"
#define PREF_TOKEN @"token"
#define PREF_ID @"id"

#endif /* ConstantParam_h */
