//
//  ProfileVC.m
//  SDPAClient
//
//  Created by Sapana Ranipa on 01/01/16.
//  Copyright © 2016 Sapana Ranipa. All rights reserved.
//

#import "ProfileVC.h"

@interface ProfileVC ()
{
    CGRect SubmitOrignalFrame;
    NSMutableDictionary *UserData;
}
@end

@implementation ProfileVC

- (void)viewDidLoad
{
    [super viewDidLoad];
     SubmitOrignalFrame=self.btnSubmit.frame;
    [self DisableTextFieldMethods];
    [self SetAllDataInit];
    [self SetData];
   
    // Do any additional setup after loading the view.
}
-(void)SetAllDataInit
{
    self.btnSubmit.tag=0;
    self.btnSubmit.frame=CGRectMake(self.btnSubmit.frame.origin.x, self.viewForPassword.frame.origin.y+15, self.btnSubmit.frame.size.width, self.btnSubmit.frame.size.height);
    [self.btnSubmit setTitle:NSLocalizedString(@"EDIT_PROFILE", nil) forState:UIControlStateNormal];
    [self.btnSubmit setTitle:NSLocalizedString(@"EDIT_PROFILE", nil) forState:UIControlStateSelected];
    self.scrollObj.contentSize=CGSizeMake(self.view.frame.size.width, 530);
    self.viewForPassword.hidden=YES;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self SetLocalization];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
#pragma mark - Localization Methods
-(void)SetLocalization
{
    [self.btnMenu setTitle:NSLocalizedString(@"PROFILE",nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"PROFILE",nil) forState:UIControlStateSelected];
    [self.btnChangePassword setTitle:NSLocalizedString(@"CHANGE_PASSWORD",nil) forState:UIControlStateNormal];
    [self.btnChangePassword setTitle:NSLocalizedString(@"CHANGE_PASSWORD",nil) forState:UIControlStateSelected];
    
    [self.txtName setValue:[UIColor colorWithRed:97.0/255.0 green:97.0/255.0 blue:97.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtUserName setValue:[UIColor colorWithRed:97.0/255.0 green:97.0/255.0 blue:97.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtEmail setValue:[UIColor colorWithRed:97.0/255.0 green:97.0/255.0 blue:97.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtMobile setValue:[UIColor colorWithRed:97.0/255.0 green:97.0/255.0 blue:97.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtCurrentPassword setValue:[UIColor colorWithRed:97.0/255.0 green:97.0/255.0 blue:97.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtNewPassword setValue:[UIColor colorWithRed:97.0/255.0 green:97.0/255.0 blue:97.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtConfirmPassword setValue:[UIColor colorWithRed:97.0/255.0 green:97.0/255.0 blue:97.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtMCLiacenceNumber setValue:[UIColor colorWithRed:97.0/255.0 green:97.0/255.0 blue:97.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtNumberPlate setValue:[UIColor colorWithRed:97.0/255.0 green:97.0/255.0 blue:97.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    
    self.txtName.placeholder=NSLocalizedString(@"FULL_NAME", nil);
    self.txtUserName.placeholder=NSLocalizedString(@"USER_NAME", nil);
    self.txtEmail.placeholder=NSLocalizedString(@"EMAIL", nil);
    self.txtMobile.placeholder=NSLocalizedString(@"MOBILE", nil);
    self.txtMCLiacenceNumber.placeholder=NSLocalizedString(@"LICENCE_NUMBER", nil);
    self.txtNumberPlate.placeholder=NSLocalizedString(@"PLATE_NUMBER", nil);
    self.txtCurrentPassword.placeholder=NSLocalizedString(@"CURRENT_PASSWORD", nil);
    self.txtNewPassword.placeholder=NSLocalizedString(@"NEW_PASSWORD", nil);
    self.txtConfirmPassword.placeholder=NSLocalizedString(@"CONFIRM_PASSWORD", nil);
    
    self.lblName.text=NSLocalizedString(@"PROFILE_NAME", nil);
    self.lblUserName.text=NSLocalizedString(@"PROFILE_USERNAME", nil);
    self.lblEmail.text=NSLocalizedString(@"PROFILE_EMAIL", nil);
    self.lblMobile.text=NSLocalizedString(@"PROFILE_MOBILE", nil);
    self.lblMCLiacenceNumber.text=NSLocalizedString(@"MC_LICENSE_NUMBER", nil);
    self.lblNumberPlate.text=NSLocalizedString(@"PROFILE_LICANCE_PLATE", nil);
    self.lblCurrentPassword.text=NSLocalizedString(@"PROFILE_CURRENT_PASSWORD", nil);
    self.lblNewPassword.text=NSLocalizedString(@"PROFILE_NEW_PASSWORD", nil);
    self.lblConfirmPassword.text=NSLocalizedString(@"PROFILE_CONFIRM_PASSWORD", nil);
}
-(void)SetData
{
    UserData=[NSPref GetPreference:PREF_LOGIN_OBJECT];
    
    [self.imgProfile applyRoundedCornersFull];
    [self.imgProfile downloadFromURL:[UserData valueForKey:@"picture"] withPlaceholder:nil];
    
    self.lblFullName.text=[UserData valueForKey:@"name"];
    self.txtUserName.text=[UserData valueForKey:@"name"];
    self.txtUserName.text=[UserData valueForKey:@"username"];
    self.txtName.text=[UserData valueForKey:@"name"];
    self.txtEmail.text=[UserData valueForKey:@"email"];
    self.txtMobile.text=[UserData valueForKey:@"phone"];
    self.txtMCLiacenceNumber.text=[UserData valueForKey:@"mc_license"];
    //self.txtNumberPlate.text=[UserData valueForKey:@"licence_plate"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark - UIbutton Action Methods

- (IBAction)onClickBtnMenu:(id)sender
{
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onClickBtnChangePassword:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if(btn.tag==0)
    {
        btn.tag=1;
        [UIView animateWithDuration:0.5 animations:^{
            self.btnSubmit.frame=SubmitOrignalFrame;
            self.scrollObj.contentSize=CGSizeMake(self.view.frame.size.width, 725);
            self.viewForPassword.hidden=NO;
        } completion:^(BOOL finished)
         {
             
         }];
    }
    else
    {
        btn.tag=0;
        [UIView animateWithDuration:0.5 animations:^{
            self.viewForPassword.hidden=YES;
            self.btnSubmit.frame=CGRectMake(self.btnSubmit.frame.origin.x, self.viewForPassword.frame.origin.y+15, self.btnSubmit.frame.size.width, self.btnSubmit.frame.size.height);
            self.scrollObj.contentSize=CGSizeMake(self.view.frame.size.width, 530);
        } completion:^(BOOL finished)
         {
         }];
    }
    
}

- (IBAction)onClickBtnSubmit:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if(btn.tag==0)
    {
        btn.tag=1;
        [self.btnSubmit setTitle:NSLocalizedString(@"UPDATE_PROFILE", nil) forState:UIControlStateNormal];
        [self.btnSubmit setTitle:NSLocalizedString(@"UPDATE_PROFILE", nil) forState:UIControlStateSelected];
        [self EnableTextFieldMethods];
    }
    else
    {
        [self CheckValidation];
    }
}

- (IBAction)onClickBtnProfilePic:(id)sender
{
    UIActionSheet *action=[[UIActionSheet alloc]initWithTitle:Nil delegate:self cancelButtonTitle:NSLocalizedString(@"CANCEL", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"TAKE_PHOTO", nil) ,NSLocalizedString(@"SELECT_IMAGE", nil),NSLocalizedString(@"REMOVE_PROFILE", nil) , nil];
    action.tag=100;
    [action showInView:self.view];
}
#pragma mark -
#pragma mark - UIActionSheet delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(actionSheet.tag==100)
    {
        switch (buttonIndex)
        {
            case 0:
                [self openCamera];
                break;
            case 1:
                [self chooseFromLibaray];
                break;
            case 2:
                self.imgProfile.image=[UIImage imageNamed:@"user"];
                break;
            case 3:
                break;
        }
    }
}

-(void)openCamera
{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        
        imagePickerController.delegate =self;
        imagePickerController.allowsEditing=YES;
        
        imagePickerController.view.tag = 102;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePickerController animated:YES completion:^{
            
        }];
    }
    else
    {
        [[UtilityClass sharedObject] showAlertWithTitle:nil andMessage:NSLocalizedString(@"CAN_NOT_AVAILABLE", nil)];
    }
}

-(void)chooseFromLibaray
{
    // Set up the image picker controller and add it to the view
    
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    
    imagePickerController.delegate =self;
    
    imagePickerController.allowsEditing=YES;
    imagePickerController.view.tag = 102;
    
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePickerController animated:YES completion:^{
    }];
}
#pragma mark -
#pragma mark - UIImagePickerController Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    self.imgProfile.image=[info objectForKey:UIImagePickerControllerEditedImage];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark - Enable Disable Methods

-(void)EnableTextFieldMethods
{
    self.btnProfilePic.enabled=YES;
    self.btnChangePassword.enabled=YES;
    
    self.txtName.enabled=YES;
    self.txtEmail.enabled=YES;
    self.txtUserName.enabled=NO;
    self.txtMobile.enabled=YES;
    self.txtMCLiacenceNumber.enabled=YES;
    //self.txtNumberPlate.enabled=YES;
    self.txtCurrentPassword.enabled=YES;
    self.txtNewPassword.enabled=YES;
    self.txtConfirmPassword.enabled=YES;
}
-(void)DisableTextFieldMethods
{
    self.btnProfilePic.enabled=NO;
    self.btnChangePassword.enabled=NO;
    
    self.txtName.enabled=NO;
    self.txtUserName.enabled=NO;
    self.txtEmail.enabled=NO;
    self.txtMobile.enabled=NO;
    self.txtMCLiacenceNumber.enabled=NO;
    //self.txtNumberPlate.enabled=NO;
    self.txtCurrentPassword.enabled=NO;
    self.txtNewPassword.enabled=NO;
    self.txtConfirmPassword.enabled=NO;
}

#pragma mark - 
#pragma mark - WebServices Methods

-(void)CheckValidation
{
    [self.view endEditing:YES];
    if(self.txtName.text.length<1 || self.txtUserName.text.length<1 || self.txtEmail.text.length<1 || ![[UtilityClass sharedObject]isValidEmailAddress:self.txtEmail.text] || self.txtMobile.text.length<1 || self.txtMCLiacenceNumber.text.length<1)
    {
        if(self.txtName.text.length<1)
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_ENTER_NAME", nil)];
        }
        else if(self.txtUserName.text.length<1)
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_ENTER_USERNAME", nil)];
        }
        else if(self.txtEmail.text.length<1)
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_ENTER_EMAIL", nil)];
        }
        else if (![[UtilityClass sharedObject]isValidEmailAddress:self.txtEmail.text])
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_VALID_EMAIL", nil)];
        }
        else if (self.txtMobile.text.length<1)
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_ENTER_MOBILE", nil)];
        }
        else if (self.txtMCLiacenceNumber.text.length<1)
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_ENTER_LICENCE_NUMBER", nil)];
        }
       /* else if (self.txtNumberPlate.text.length<1)
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_ENTER_PLATE_NUMBER", nil)];
        } */
        
    }
    else if (self.txtNewPassword.text.length > 0 || self.txtConfirmPassword.text.length > 0)
    {
        if ([self.txtNewPassword.text isEqualToString:self.txtConfirmPassword.text])
        {
            [self updateProfile];
        }
        else
        {
            [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(@"DO_NOT_MATCH_CONFIRM_PASSWORD",nil)];
        }
    }
    else
    {
        [self updateProfile];
    }
}
-(void)updateProfile
{
    [self.view endEditing:YES];
    if([[AppDelegate sharedAppDelegate] connected])
    {
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"UPDATING", nil)];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKey:PARAM_ID];
        [dictParam setObject:self.txtName.text forKey:PARAM_NAME];
        [dictParam setObject:self.txtUserName.text forKey:PARAM_NAME];
        [dictParam setObject:self.txtEmail.text forKey:PARAM_EMAIL];
        [dictParam setObject:self.txtCurrentPassword.text forKey:PARAM_OLD_PASSWORD];
        [dictParam setObject:self.txtNewPassword.text forKey:PARAM_NEW_PASSWORD];
        [dictParam setObject:self.txtMobile.text forKey:PARAM_PHONE];
        [dictParam setObject:self.txtMCLiacenceNumber.text forKey:PARAM_LICENCE];
        [dictParam setObject:@"" forKey:PARAM_PLATE];
        
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        UIImage *uploadImage=[[UtilityClass sharedObject]scaleAndRotateImage:self.imgProfile.image];
        [afn getDataFromPath:P_UPDATE_PROFILE withParamDataImage:dictParam andImage:uploadImage withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 [[AppDelegate sharedAppDelegate] hideLoadingView];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [[AppDelegate sharedAppDelegate] showToastMessage:NSLocalizedString(@"PROFILE_UPDATE_SUCCESSFULLY", nil)];
                     [NSPref SetPreference:PREF_LOGIN_OBJECT Value:response];
                     self.txtCurrentPassword.text = @"";
                     self.txtConfirmPassword.text = @"";
                     self.txtNewPassword.text = @"";
                     [self DisableTextFieldMethods];
                     [self SetData];
                     [self SetAllDataInit];
                     self.btnChangePassword.tag=0;
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     if([str isEqualToString:@"21"])
                     {
                         [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                     }
                     else
                     {
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
#pragma mark -
#pragma mark - UITextFields Delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField==self.txtMobile)
    {
        NSCharacterSet *nonNumberSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        return ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0) || [string isEqualToString:@""];
    }
    return YES;
}
@end
