//
//  HistoryVC.h
//  SDPAClient
//
//  Created by Sapana Ranipa on 01/01/16.
//  Copyright © 2016 Sapana Ranipa. All rights reserved.
//

#import "BaseVC.h"
#import "HistoryCell.h"

@interface HistoryVC : BaseVC<UITableViewDataSource,UITableViewDelegate>
{

}

@property (weak, nonatomic) IBOutlet UIImageView *img_no_display;
@property (weak, nonatomic) IBOutlet UITableView *tableForHistory;


@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
- (IBAction)onClickBtnMenu:(id)sender;


@property (weak, nonatomic) IBOutlet UIView *viewForDetail;

@property (weak, nonatomic) IBOutlet UIImageView *imgClientPro;

@property (weak, nonatomic) IBOutlet UILabel *lblClientName;
@property (weak, nonatomic) IBOutlet UILabel *lblCarTypeYear;
@property (weak, nonatomic) IBOutlet UILabel *lblPickupTime;
@property (weak, nonatomic) IBOutlet UILabel *lblPickup;
@property (weak, nonatomic) IBOutlet UILabel *lblPickupAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDropoff;
@property (weak, nonatomic) IBOutlet UILabel *lblDropoffAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblInvoice;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalPayment;
@property (weak, nonatomic) IBOutlet UILabel *lblOperational;
@property (weak, nonatomic) IBOutlet UILabel *lblTransport;
@property (weak, nonatomic) IBOutlet UILabel *lblSetOperational;
@property (weak, nonatomic) IBOutlet UILabel *lblSetTransport;

@property (weak, nonatomic) IBOutlet UITextView *textViewForDescription;

@property (weak, nonatomic) IBOutlet UIButton *btnClose;
- (IBAction)onClickBtnClose:(id)sender;




@end
