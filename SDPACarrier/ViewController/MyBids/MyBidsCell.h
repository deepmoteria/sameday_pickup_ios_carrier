//
//  MyBidsCell.h
//  SDPACarrier
//
//  Created by Sapana Ranipa on 13/01/16.
//  Copyright © 2016 Elluminati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyBidsCell : UITableViewCell
{

}
//@property (weak, nonatomic) IBOutlet UIImageView *userProfile;

@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblCarTypeYear;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblYouBided;

@property (weak, nonatomic) IBOutlet UIButton *btnMessage;
@end
