//
//  DistanceCell.h
//  SDPACarrier
//
//  Created by Sapana Ranipa on 09/01/16.
//  Copyright © 2016 Elluminati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DistanceCell : UITableViewCell
{

}
@property (weak, nonatomic) IBOutlet UILabel *lblViewBidsNearBy;
@property (weak, nonatomic) IBOutlet UILabel *lblType;
@end
