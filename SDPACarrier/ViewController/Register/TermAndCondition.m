//
//  TermAndCondition.m
//  
//
//  Created by Sapana Ranipa on 29/12/15.
//
//

#import "TermAndCondition.h"

@interface TermAndCondition ()

@end

@implementation TermAndCondition

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO];
    [self.btnAccept setTitle:NSLocalizedString(@"ACCEPT", nil) forState:UIControlStateNormal];
    [self.btnAccept setTitle:NSLocalizedString(@"ACCEPT", nil) forState:UIControlStateHighlighted];
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
    self.navigationItem.hidesBackButton=YES;
    NSURL *websiteUrl = [NSURL URLWithString:PRIVACY_URL];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:websiteUrl];
    [self.webObj loadRequest:urlRequest];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [APPDELEGATE hideLoadingView];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onClickBtnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
