//
//  TermAndCondition.h
//  
//
//  Created by Sapana Ranipa on 29/12/15.
//
//

#import "BaseVC.h"

@interface TermAndCondition : BaseVC<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webObj;

@property (weak, nonatomic) IBOutlet UIButton *btnAccept;

- (IBAction)onClickBtnBack:(id)sender;
@end
