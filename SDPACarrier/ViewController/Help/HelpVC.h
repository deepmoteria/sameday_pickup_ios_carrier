//
//  HelpVC.h
//  SDPACarrier
//
//  Created by Sapana Ranipa on 08/01/16.
//  Copyright © 2016 Elluminati. All rights reserved.
//

#import "BaseVC.h"
#import <MessageUI/MessageUI.h>

@interface HelpVC : BaseVC<UIWebViewDelegate,MFMailComposeViewControllerDelegate>
{

}
@property (weak, nonatomic) IBOutlet UIWebView *webObj;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UILabel *lblHavingProblems;
@property (weak, nonatomic) IBOutlet UILabel *lblContactMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblMobile;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;

- (IBAction)onClickBtnMenu:(id)sender;
- (IBAction)onClickBtnCall:(id)sender;
- (IBAction)onClickBtnEmail:(id)sender;

@end
