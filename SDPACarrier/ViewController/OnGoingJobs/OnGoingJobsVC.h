//
//  OnGoingJobsVC.h
//  SDPACarrier
//
//  Created by Sapana Ranipa on 13/01/16.
//  Copyright © 2016 Elluminati. All rights reserved.
//

#import "BaseVC.h"
#import "AvailableBidsVC.h"
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface OnGoingJobsVC : BaseVC<CLLocationManagerDelegate,GMSMapViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate>
{
    CLLocationManager *locationManager;
    GMSMapView *mapView_;
}
@property (weak, nonatomic) IBOutlet UIView *viewForMenu;
@property (weak, nonatomic) IBOutlet UICollectionView *CollectionObj;
@property (weak, nonatomic) AvailableBidsVC *viewObj;
@property(strong,nonatomic) NSMutableDictionary *ClientData;
@property (weak, nonatomic) IBOutlet UIView *mapView;

@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnActionForWalk;

- (IBAction)onClickBtnMenu:(id)sender;
- (IBAction)onClickBtnActionForWalk:(id)sender;
- (IBAction)onClickBtnClientCall:(id)sender;

// viewForClientInfo
@property (weak, nonatomic) IBOutlet UIImageView *imgClientProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblClientName;
@property (weak, nonatomic) IBOutlet UILabel *lblClientCarTypeYear;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;

@property (weak, nonatomic) IBOutlet UIButton *btnViewHideShow;
- (IBAction)onClickBtnViewStatus:(id)sender;

- (IBAction)onClickBtnHideShow:(id)sender;
// ViewForDetail

@property (weak, nonatomic) IBOutlet UIView *viewForDetails;
@property (weak, nonatomic) IBOutlet UITextView *textViewForDescription;

@property (weak, nonatomic) IBOutlet UILabel *lblPickUp;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUpAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOff;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblOperational;
@property (weak, nonatomic) IBOutlet UILabel *lblTransport;
@property (weak, nonatomic) IBOutlet UILabel *lblSetOperational;
@property (weak, nonatomic) IBOutlet UILabel *lblSetTransport;


// ViewForStatus

@property (weak, nonatomic) IBOutlet UIView *viewForStatus;

@property (weak, nonatomic) IBOutlet UILabel *lblPick1;
@property (weak, nonatomic) IBOutlet UILabel *lblPick2;
@property (weak, nonatomic) IBOutlet UILabel *lblPick3;
@property (weak, nonatomic) IBOutlet UILabel *lblDelivery1;
@property (weak, nonatomic) IBOutlet UILabel *lblDelivery2;
@property (weak, nonatomic) IBOutlet UILabel *lblDelivery3;

@property (weak, nonatomic) IBOutlet UIImageView *imgPick1;
@property (weak, nonatomic) IBOutlet UIImageView *imgPick2;
@property (weak, nonatomic) IBOutlet UIImageView *imgPick3;
@property (weak, nonatomic) IBOutlet UIImageView *imgDelivery1;
@property (weak, nonatomic) IBOutlet UIImageView *imgDelivery2;
@property (weak, nonatomic) IBOutlet UIImageView *imgDelivery3;
@end
